<?php

add_action( 'wp_enqueue_scripts', 'neve_child_enqueue', 102 );
function neve_child_enqueue () {
    wp_enqueue_style( 'neve-main-style', get_stylesheet_directory_uri() . '/style.css', false, '1.0', 'all' );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(), array( 'neve-main-style' ), '1.1.4');
}

add_action('after_setup_theme', 'reset_parent_setup', 999);

function reset_parent_setup()
{
    add_image_size( 'neve-blog', 1024, 768, true );
}

